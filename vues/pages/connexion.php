<div class="container">
    <?php if (isset($_SESSION['alert'])) : ?>
        <div class="alert alert-<?= $_SESSION['alert']['type'] ?>" role="alert">
            <?php 
                if(is_array($_SESSION['alert']['message'])){
                    foreach($_SESSION['alert']['message'] as $row){
                        echo $row."<br>";
                    }
                }else{
                    echo $_SESSION['alert']['message'];
                }
            ?>

        </div>
    <?php
        unset($_SESSION['alert']);
    endif;
    ?>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-4">
            <h1>Connexion</h1>
            <form action="index.php?route=connexion" method="POST">
                <div>
                    <label for="email">Email</label>
                    <input type="text" name="email" class="form-control">
                </div>
                <div>
                    <label for="mdp">Mot de passe</label>
                    <input type="password" name="mdp" class="form-control">
                </div>
                <div class="mt-2">
                    <input type="submit" value="Connexion" class="btn btn-info">
                </div>
            </form>
        </div>
    </div>
</div>