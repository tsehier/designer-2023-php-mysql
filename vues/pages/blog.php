<?php $articles = $data['articles']; ?>
<div class="hero">
    <h1>Blog</h1>
</div>
<div class="container">
    <?php if (isset($_SESSION['alert'])) : ?>
        <div class="alert alert-<?= $_SESSION['alert']['type'] ?>" role="alert">
            <?= $_SESSION['alert']['message'] ?>
        </div>
    <?php
        unset($_SESSION['alert']);
    endif;
    ?>
</div>
<section class="container blog-section">

    <?php for ($increment = 0; $increment <= count($articles) - 1; $increment++) : ?>
        <article class="p-2 rounded border shadow-sm">
            <div>
                <img class="rounded img-fluid" src="<?php echo $articles[$increment]['image'] ?>" alt="">
            </div>
            <h2><?php echo $articles[$increment]['titre'] ?></h2>
            <p><?php echo substr($articles[$increment]['contenu'], 0, 140); ?></p>
            <div>
                <span><?php echo $articles[$increment]['auteur'] ?></span>
                <span><?php echo $articles[$increment]['date'] ?></span>
            </div>
            <div class="article-actions">
                <a href="index.php?route=article&id=<?php echo $articles[$increment]['id'] ?>" class="btn btn-info">Lire la suite</a>
            </div>
        </article>
    <?php endfor; ?>
</section>