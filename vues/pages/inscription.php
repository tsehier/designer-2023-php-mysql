<div class="container">
    <?php if (isset($_SESSION['alert'])) : ?>
        <div class="alert alert-<?= $_SESSION['alert']['type'] ?>" role="alert">
            <?= $_SESSION['alert']['message'] ?>
        </div>
    <?php
        unset($_SESSION['alert']);
    endif;
    ?>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-4 my-2">
            <h1>Inscription</h1>
            <form action="" method="POST">
                <div>
                    <label for="prenom">Prénom</label>
                    <input type="text" name="prenom" class="form-control">
                </div>
                <div>
                    <label for="nom">Nom</label>
                    <input type="text" name="nom" class="form-control">
                </div>
                <div>
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control">
                </div>
                <div>
                    <label for="mdp">Mot de passe</label>
                    <input type="password" name="mdp" class="form-control">
                </div>
                <div class="mt-2">
                    <input type="submit" value="Inscription" class="form-control btn btn-success">
                </div>

            </form>
        </div>
    </div>
</div>