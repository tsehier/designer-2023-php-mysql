<div class="container-fluid">
    <div class="row">
        <?php include('vues/admin/aside.php') ?>
        <section class="col-9">
            <?php if(isset($_SESSION['alert'])) : ?>
                <div class="alert alert-<?= $_SESSION['alert']['type'] ?>" role="alert">
                    <?= $_SESSION['alert']['message'] ?>
                </div>
            <?php
                unset($_SESSION['alert']);
            endif;
            ?>
        </section>
    </div>
</div>