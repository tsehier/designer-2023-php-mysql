<div class="container-fluid">
    <div class="row">
        <?php include('vues/admin/aside.php') ?>
        <section class="col-9">
            <h1><?php echo $data['title'] ?></h1>
            <form action="#" method="POST">
                <p>
                    <label for="titre">Titre</label>
                    <input type="text" name="titre" id="titre" value="<?php echo $data['article']['titre'] ?>">
                </p>
                <p>
                    <label for="auteur">Auteur</label>
                    <input type="text" name="auteur" id="auteur" value="<?php echo $data['article']['auteur'] ?>">
                </p>
                <p>
                    <label for="date">Date</label>
                    <input type="date" name="date" id="date" value="<?php echo $data['article']['date'] ?>">
                </p>
                <p>
                    <label for="image">Image</label>
                    <input type="text" name="image" id="image" value="<?php echo $data['article']['date'] ?>">
                </p>
                <p>
                    <label for="contenu">Contenu</label>
                    <textarea name="contenu" id="contenu"><?php echo $data['article']['contenu'] ?></textarea>
                </p>
                <p>
                    <input type="submit" value="Modifer" class="btn btn-warning">
                </p>
            </form>
        </section>
    </div>
</div>