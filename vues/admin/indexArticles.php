<div class="container-fluid">
    <div class="row">
        <?php include('vues/admin/aside.php') ?>
        <section class="col-9">
            <table class="table">
                <thead>
                    <tr>
                        <th>Titre</th>
                        <th>Auteur</th>
                        <th>Date</th>
                        <th>Action(s)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($data['articles'] as $article) : ?>
                        <tr>
                            <td><?= $article['titre']; ?></td>
                            <td><?php echo $article['auteur']; ?></td>
                            <td><?= $article['date']; ?></td>
                            <td>
                                <a class="btn btn-warning" href="index.php?route=admin&action=editArticle&id=<?= $article['id'] ?>">Modifier</a>
                                <a class="btn btn-danger" href="index.php?route=admin&action=deleteArticle&id=<?php echo $article['id'] ?>">Supprimer</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </section>
    </div>
</div>