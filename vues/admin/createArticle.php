<div class="container-fluid">
    <div class="row">
        <?php include('vues/admin/aside.php') ?>
        <section class="col-9">
            <h1>Ajouter un article</h1>
            <form action="#" method="POST">
                <div>
                    <label for="titre" class="form-label">Titre</label>
                    <input type="text" name="titre" id="titre" class="form-control">
                </div>
                <div>
                    <label for="auteur" class="form-label">Auteur</label>
                    <input type="text" name="auteur" id="auteur" class="form-control">
                </div>
                <div>
                    <label for="date" class="form-label">Date</label>
                    <input type="date" name="date" id="date" class="form-control">
                </div>
                <div>
                    <label for="image" class="form-label">Image</label>
                    <input type="text" name="image" id="image" class="form-control">
                </div>
                <div>
                    <label for="contenu" class="form-label">Contenu</label>
                    <textarea name="contenu" id="contenu" rows="10" class=" form-control"></textarea>
                </div>
                <div class="my-3 row justify-content-end">
                    <div class="col-3">
                        <input type="submit" value="Créer" class="btn btn-outline-success form-control">
                    </div>
                </div>
            </form>
        </section>
    </div>
</div>