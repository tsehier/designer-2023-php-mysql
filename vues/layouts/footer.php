<footer class="container-fluid bg-light py-2">
    <div class="container d-flex justify-content-between">
        <a class="d-block text-secondary h4" href="index.php?route=accueil">
            <span class="fw-bold">Ticket</span>Immobilier
        </a>
        <nav>
            <ul class="d-flex gap-2">

                <?php if (isset($_SESSION['sessionToken'])) : ?>
                    <li>
                        <a href="index.php?route=deconnexion" class="btn btn-danger">Déconnexion</a>
                    </li>
                <?php endif; ?>
                <li>
                    <a href="index.php?route=inscription">Inscription</a>
                </li>
                <li>
                    <a href="index.php?route=admin&action=tdb">ADMINISTRATION</a>
                </li>
            </ul>
        </nav>
    </div>
</footer>
</body>

</html>