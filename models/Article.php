<?php

class Article extends DB
{

    public $id;
    public $titre;
    public $date;
    public $auteur;
    public $contenu;
    public $image;

    public function getArticles()
    {
        $selectArticle = $this->connect()->prepare('
            SELECT * FROM articles
        ');

        $selectArticle->execute();

        return $selectArticle->fetchAll();
    }

    public function getArticle($id)
    {
        $selectOneArticle = $this->connect()->prepare('
            SELECT * FROM articles WHERE id=:id
        ');

        $selectOneArticle->execute([
            'id' => $id
        ]);

        return $selectOneArticle->fetch();
    }

    public function saveArticle()
    {
        $insertArticle = $this->connect()->prepare('
            INSERT INTO articles(
                titre,
                date,
                auteur,
                contenu,
                image
                )
            VALUES (
                :titre,
                :date,
                :auteur,
                :contenu,
                :image
            )    
        ');

        $insertArticle->execute([
            'titre' => $this->titre,
            'date' => $this->date,
            'auteur' => $this->auteur,
            'contenu' => $this->contenu,
            'image' => $this->image
        ]);
    }

    public function editArticle()
    {
        $updateArticle = $this->connect()->prepare('
            UPDATE articles
            SET
            titre = :titre,
            date = :date,
            auteur = :auteur,
            contenu = :contenu,
            image = :image
            WHERE
            id = :id
        ');

        $updateArticle->execute([
            'id' => $this->id,
            'titre' => $this->titre,
            'date' => $this->date,
            'auteur' => $this->auteur,
            'contenu' => $this->contenu,
            'image' => $this->image
        ]);
    }

    public function deleteArticle()
    {
        $deleteArticle = $this->connect()->prepare('
            DELETE FROM articles
            WHERE id = :id
        ');

        $deleteArticle->execute([
            'id' => $this->id
        ]);
    }
}
