<?php

class User extends DB
{
    public $id;
    public $prenom;
    public $nom;
    public $email;
    public $mdp;
    public $isAdmin;
    public $sessionToken;

    public function saveUser()
    {
        $insertUser = $this->connect()->prepare('
            INSERT INTO users(
                prenom,
                nom,
                email,
                mdp,
                isAdmin,
                sessionToken
                )
            VALUES (
                :prenom,
                :nom,
                :email,
                :mdp,
                :isAdmin,
                :sessionToken
                )    
        ');

        $insertUser->execute([
            'prenom' => $this->prenom,
            'nom' => $this->nom,
            'email' => $this->email,
            'mdp' => password_hash($this->mdp, PASSWORD_BCRYPT),
            'isAdmin' => $this->isAdmin,
            'sessionToken' => $this->sessionToken
        ]);
    }

    public function login()
    {
        $user = $this->getUserByEmail($this->email);

        if ($user != false) {
            if (password_verify($this->mdp, $user['mdp'])) {
                $this->setSessionToken($user['id']);
            }else{
                $_SESSION['alert'] = [
                    'type' => 'danger',
                    'message' => 'Mot de passe incorrect'
                ];
            
                header('Location: index.php?route=connexion');
                die();
            }
        }else{
            $_SESSION['alert'] = [
                'type' => 'danger',
                'message' => 'Email inconnu'
            ];

            header('Location: index.php?route=connexion');
            die();
        }
    }

    public function getUserByEmail($email)
    {
        $selectUser = $this->connect()->prepare('
            SELECT * FROM users
            WHERE email = :email
        ');

        $selectUser->execute([
            'email' => $email
        ]);

        return $selectUser->fetch();
    }

    private function setSessionToken($id)
    {
        $sessionToken = Tool::getRandomString(16);

        $updateToken = $this->connect()->prepare('
            UPDATE users SET 
            sessionToken = :sessionToken
            WHERE id = :id
        ');

        $updateToken->execute([
            'sessionToken' => $sessionToken,
            'id' => $id
        ]);

        $_SESSION['sessionToken'] = $sessionToken;
    }

    public function getUserBySessionToken($sessionToken)
    {
        $selectUser = $this->connect()->prepare('
            SELECT * FROM users
            WHERE sessionToken = :sessionToken
        ');

        $selectUser->execute([
            'sessionToken' => $sessionToken
        ]);

        return $selectUser->fetch();
    }

    public function deleteSessionToken($sessionToken)
    {
        unset($_SESSION['sessionToken']);

        $updateUser = $this->connect()->prepare('
            UPDATE users 
            SET sessionToken = :nullSessionToken
            WHERE sessionToken = :oldSessionToken
        ');

        $updateUser->execute([
            'nullSessionToken' => null,
            'oldSessionToken' => $sessionToken
        ]);
    }

    public function verifSessionToken(){
        $dbSessionToken = $this->connect()->prepare('
            SELECT * FROM USERS
            WHERE sessionToken = :sSessionToken
        ');

        $dbSessionToken->execute([
            'sSessionToken' => $this->sessionToken
        ]);

        $verifUser = $dbSessionToken->fetch();

        if($verifUser != false){
            return true;
        }else{
            return false;
        }

    }
}
