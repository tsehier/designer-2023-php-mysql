<?php

class HomeController
{

    public static function home()
    {
        return [
            'title' => 'Accueil',
            'corps' => 'vues/pages/accueil.php'
        ];
    }

    public static function contact()
    {
        return [
            'title' => 'Contactez nous',
            'corps' => 'vues/pages/contact.php'
        ];
    }

    public static function error(){
        header("HTTP/1.0 404 Not Found");
        return [
            'title' => 'Page introuvable',
            'corps' => 'vues/pages/error.php'
        ];
    }
}
