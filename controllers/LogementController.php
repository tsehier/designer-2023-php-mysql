<?php

class LogementController
{

    public static function index()
    {
        return [
            'title' => 'Logements',
            'corps' => 'vues/pages/logements.php'
        ];
    }
}
