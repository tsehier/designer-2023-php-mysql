<?php

class UserController
{

    public static function inscription()
    {
        return [
            'title' => 'Inscription',
            'corps' => 'vues/pages/inscription.php'
        ];
    }

    public static function store($data)
    {
        $user = new User();
        $user->prenom = $data['prenom'];
        $user->nom = $data['nom'];
        $user->email = $data['email'];
        $user->mdp = $data['mdp'];
        $user->isAdmin = 0;
        $user->sessionToken = null;

        $userInDB = new User();
        $testIfUserExist = $userInDB->getUserByEmail($user->email);

        if ($testIfUserExist != false) {

            $_SESSION['alert'] = [
                'type' => 'danger',
                'message' => "L'email existe déjà"
            ];

            header('Location: index.php?route=inscription');
            die();
        }

        $user->saveUser();

        $user->login();

        $_SESSION['alert'] = [
            'type' => 'success',
            'message' => 'Connexion réussi'
        ];

        header('Location: index.php?route=admin&action=tdb');
    }

    public static function deconnexion()
    {
        $user = new User();
        $userConnected = $user->getUserBySessionToken($_SESSION['sessionToken']);

        if ($userConnected != false) {
            $user->deleteSessionToken($_SESSION['sessionToken']);

            $_SESSION['alert'] = [
                'type' => 'success',
                'message' => 'Déconnexion réussie'
            ];

            header('Location: index.php?route=blog');
        }
    }

    public static function connexion($redirect = false)
    {
        if($redirect){
            $_SESSION['alert'] = [
                'type' => 'danger',
                'message' => 'Vous devez être connecté'
            ];
        }

        if(self::verif()){
            header('Location: index.php?route=admin&action=tdb');
        }

        return [
            'title' => 'Page de connexion',
            'corps' => 'vues/pages/connexion.php'
        ];
    }

    public static function connect($data){
        $user = new User();
        $user->email = $data['email'];
        $user->mdp = $data['mdp'];

        $errors = [];

        if(isset($data['email']) && empty($data['email'])){
            $errors[] = 'L\'email ne doit pas être vide';
        }

        if(isset($data['mdp']) && empty($data['mdp'])){
            $errors[] = "Le mot de passe ne doit pas être vide";
        }

        if(!empty($errors)){
            $_SESSION['alert'] = [
                'type' => 'warning',
                'message' => $errors
            ];

            header('Location: index.php?route=connexion');
            die();
        }

        $user->login();

        $_SESSION['alert'] = [
            'type' => 'success',
            'message' => 'Connexion réussi'
        ];

        header('Location: index.php?route=admin&action=tdb');
    }

    public static function verif(){
        if(isset($_SESSION['sessionToken']) && !empty($_SESSION['sessionToken'])){
            $user = new User();
            $user->sessionToken = $_SESSION['sessionToken'];

            return $user->verifSessionToken();
        }else{
            return false;
        }

    }
}
