<?php
require_once 'models/Tool.php';
require_once 'models/DB.php';
require_once 'models/Article.php';
require_once 'models/User.php';

require_once 'controllers/HomeController.php';
require_once 'controllers/ArticleController.php';
require_once 'controllers/LogementController.php';
require_once 'controllers/AdminController.php';
require_once 'controllers/UserController.php';

session_start();

// Mon super router 

// Redirige vers la bonne page
switch ($_GET['route']) {
    case 'admin':
        if(UserController::verif()){
            switch ($_GET['action']) {
                case 'tdb':
                    $data = AdminController::tdb();
                    break;
                case 'indexArticles':
                    $data = ArticleController::adminIndex();
                    break;
                case 'createArticle':
                    if (isset($_POST) && !empty($_POST)) {
                        ArticleController::store($_POST);
                    }
                    $data = ArticleController::create();
                    break;
                case 'editArticle':
                    if (isset($_POST) && !empty($_POST)) {
                        ArticleController::update($_GET['id'], $_POST);
                    }
                    $data = ArticleController::edit($_GET['id']);
                    break;
                case 'deleteArticle':
                    ArticleController::delete($_GET['id']);
                    break;
                default:
                    header('Location: index.php?route=admin&action=tdb');
            }
        }else{
            $data = UserController::connexion(true);
        }
        break;
    case 'logements':
        $data = LogementController::index();
        break;
    case 'contact':
        $data = HomeController::contact();
        break;
    case 'blog':
        $data = ArticleController::index();
        break;
    case 'accueil':
        $data = HomeController::home();
        break;
    case 'article':
        $data = ArticleController::show($_GET['id']);
        break;
    case '404':
        $data = HomeController::error();
        break;
    case 'inscription':
        if (isset($_POST) and !empty($_POST)) {
            UserController::store($_POST);
        }
        $data = UserController::inscription();
        break;
    case 'deconnexion':
        UserController::deconnexion();
        break;
    case 'connexion':
        if(isset($_POST) && !empty($_POST)){
            UserController::connect($_POST);
        }
        $data = UserController::connexion();
        break;
    default:
        header('Location: index.php?route=accueil');
        // A NE PAS FAIRE = include('vues/pages/logements.php');
}
include("vues/layouts/header.php");
include($data['corps']);
include("vues/layouts/footer.php");
