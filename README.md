# Designer 2023 - PHP / MySql

## Objectif final

### Page d'accueil

![Page d'accueil](doc/img/IMG_0858.jpg)

### Page de bien

![Page de bien](doc/img/IMG_0857.jpg)

## PHP

### Différence entre HTML et PHP

![HTML et PHP](doc/img/Diff%20HTML%20et%20PHP.png)

### Type de variable

| Type   | Correspondance      |
| ------ | ------------------- |
| Int    | Nombre entier       |
| Float  | Nombre décimal      |
| String | Chaîne de caractère |
| Array  | Tableau             |

### Afficher ou débugger une variable

Variable de type int, float ou string

```php
<?php echo $mavariable ?> // écriture longue
<?= $mavariable ?> // écriture raccourci
```

Variable de type array

```php
<?php var_dump($mavariable) ?>
```

### Conditions

```php
<?php
    switch($mavariable){
        case 1:
            echo "$mavaribale vaut 1";
            break;
        case 2:
            echo "$mavaribale vaut 2";
            break;
        default:
            echo "$mavaribale vaut n'importe quoi autre que 1 ou 2";
    }
?>
```

### Boucle

```php
<?php
    for($i = 0; $i <= 10; $i++){

    }
?>
```

### Intégration de PHP avec du HTML

2 écritures sont possibles, mais procurent exactement le même résultat

1. Rester toujours dans la même balise PHP

```php
<?php
    $articles = []; //Le tableau "articles" contient l'ensemble de mes articles

    for($i = 0; $i <= count($article)-1;$i++){
        echo "<h1>".$article[$i]['titre']."</h1>";
        echo "<p>".$article[$i]['contenu']."</p>";
    }
?>
```

2. Écrire dans plusieurs balises PHP

```php
<?php
    $articles = []; //Le tableau "articles" contient l'ensemble de mes articles

    for($i = 0; $i <= count($articles)-1;$i++):
?>
    <h1><?php echo $article[$i]['titre'] ?></h1>
    <p><?php echo $article[$i]['contenu'] ?></p>
<?php
    endfor;
?>
```

### Fonctions

#### Count

Compte tous les éléments d'un tableau

```php
<?php count($mavariable) ?>
```

#### Substr

Retourne un segment de chaîne

```php
<?php substr($mavarible, 0, 144) ?>
```
